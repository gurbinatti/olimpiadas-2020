package br.com.guilherme.olimpiadas.config.validacao;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErroGenerico extends RuntimeException{
    private String campo;
    private String erro;
}
