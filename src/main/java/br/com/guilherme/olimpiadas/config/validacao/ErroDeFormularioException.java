package br.com.guilherme.olimpiadas.config.validacao;

import lombok.Getter;

@Getter
public class ErroDeFormularioException extends ErroGenerico{
    public ErroDeFormularioException(String campo, String erro) {
        super(campo, erro);
    }
}
