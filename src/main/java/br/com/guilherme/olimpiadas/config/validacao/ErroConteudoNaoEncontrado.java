package br.com.guilherme.olimpiadas.config.validacao;

import lombok.Getter;

@Getter
public class ErroConteudoNaoEncontrado extends ErroGenerico{
    public ErroConteudoNaoEncontrado(String campo, String erro) {
        super(campo, erro);
    }
}
