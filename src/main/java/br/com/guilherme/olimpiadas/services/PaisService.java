package br.com.guilherme.olimpiadas.services;

import br.com.guilherme.olimpiadas.config.validacao.ErroConteudoNaoEncontrado;
import br.com.guilherme.olimpiadas.config.validacao.ErroDeFormularioException;
import br.com.guilherme.olimpiadas.controller.dto.PaisDto;
import br.com.guilherme.olimpiadas.controller.form.PaisForm;
import br.com.guilherme.olimpiadas.models.Pais;
import br.com.guilherme.olimpiadas.repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PaisService {

    @Autowired
    private PaisRepository paisRepository;

    public Page<PaisDto> buscarPaises(String descricaoPais, Pageable paginacao) {
        if (descricaoPais == null) {
            return PaisDto.converter(paisRepository.findAll(paginacao));
        }
        return PaisDto.converter(paisRepository.findByDescricaoContains(descricaoPais, paginacao));
    }

    public PaisDto inserirPais(PaisForm formPais) {
        Long paisId = 0L;
        validaPaisJaExiste(paisId,formPais.getDescricao());
        Pais pais = formPais.converter();
        paisRepository.save(pais);
        return new PaisDto(pais);
    }

    public Pais retornarPais(Long id){
        return buscarPais(id);
    }

    public Pais atualizarPais(Long id, PaisForm formPais){
       validaPaisJaExiste(id,formPais.getDescricao());
        buscarPais(id);
        return formPais.atualizar(id,paisRepository);
    }

    public void excluirPais(Long id){
        buscarPais(id);
        paisRepository.deleteById(id);
    }

    private Pais buscarPais(Long id) {
        Optional<Pais> pais = paisRepository.findById(id);
        if (pais.isPresent()) {
            return pais.get();
        }
        throw new ErroConteudoNaoEncontrado("id", "Pais nao encontrado");
    }

    public void validaPaisJaExiste(Long paisId, String paisDescricao) {
        Optional<Pais> pais = paisRepository.findByDescricao(paisDescricao);
        if (!pais.isEmpty()){
            if (pais.get().getId() != paisId && pais.isPresent()){
                throw new ErroDeFormularioException("descricao", "Descricao ja existe");
            }
        }
    }
}
