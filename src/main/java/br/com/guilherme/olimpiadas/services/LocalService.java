package br.com.guilherme.olimpiadas.services;

import br.com.guilherme.olimpiadas.config.validacao.ErroConteudoNaoEncontrado;
import br.com.guilherme.olimpiadas.config.validacao.ErroDeFormularioException;
import br.com.guilherme.olimpiadas.controller.dto.LocalDto;
import br.com.guilherme.olimpiadas.controller.form.LocalForm;
import br.com.guilherme.olimpiadas.models.Local;
import br.com.guilherme.olimpiadas.repository.LocalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LocalService {

    @Autowired
    private LocalRepository localRepository;

    public Page<LocalDto> buscarLocais(String descricaoLocal, Pageable paginacao) {
        if (descricaoLocal == null) {
            return LocalDto.converter(localRepository.findAll(paginacao));
        }
        return LocalDto.converter(localRepository.findByDescricaoContains(descricaoLocal, paginacao));
    }

    public LocalDto inserirLocal(LocalForm formLocal) {
        Long localId = 0L;
        validaLocalJaExiste(localId,formLocal.getDescricao());
        Local local = formLocal.converter();
        localRepository.save(local);
        return new LocalDto(local);
    }

    public Local retornarLocal(Long id){
        return buscarLocal(id);
    }

    public Local atualizarLocal(Long id, LocalForm formLocal){
        validaLocalJaExiste(id,formLocal.getDescricao());
        buscarLocal(id);
        return formLocal.atualizar(id,localRepository);
    }

    public void excluirLocal(Long id){
        buscarLocal(id);
        localRepository.deleteById(id);
    }

    private Local buscarLocal(Long id) {
        Optional<Local> local = localRepository.findById(id);
        if (local.isPresent()) {
            return local.get();
        }
        throw new ErroConteudoNaoEncontrado("id", "Local nao encontrado");
    }

    public void validaLocalJaExiste(Long localId, String localDescricao) {
        Optional<Local> local = localRepository.findByDescricao(localDescricao);
        if (!local.isEmpty()){
            if (local.get().getId() != localId && local.isPresent()){
                throw new ErroDeFormularioException("descricao", "Descricao ja existe");
            }
        }
    }

}
