package br.com.guilherme.olimpiadas.services;

import br.com.guilherme.olimpiadas.config.validacao.ErroConteudoNaoEncontrado;
import br.com.guilherme.olimpiadas.controller.dto.CompeticaoDto;
import br.com.guilherme.olimpiadas.controller.form.CompeticaoForm;
import br.com.guilherme.olimpiadas.models.*;
import br.com.guilherme.olimpiadas.repository.CompeticaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class CompeticaoService {
    @Autowired
    CompeticaoRepository competicaoRepository;

    @Autowired
    LocalService localService;

    @Autowired
    EtapaService etapaService;

    @Autowired
    ModalidadeService modalidadeService;

    @Autowired
    PaisService paisService;

    public Page<CompeticaoDto> buscarCompeticoes(Long modalidade, Pageable paginacao) {
        if (modalidade == null){
            return CompeticaoDto.converter(competicaoRepository.findAll(PageRequest.of(paginacao.getPageNumber(),paginacao.getPageSize(), Sort.by("dataInicio").ascending())));
        }
        return CompeticaoDto.converter(competicaoRepository.findByModalidade_Id(modalidade,paginacao));
    }

    public CompeticaoDto inserirCompeticao(CompeticaoForm formCompeticao) {
        Local local = localService.retornarLocal(formCompeticao.getLocal());
        Etapa etapa = etapaService.retornarEtapa(formCompeticao.getEtapa());
        Modalidade modalidade = modalidadeService.retornarModalidade(formCompeticao.getModalidade());
        Pais paisA  = paisService.retornarPais(formCompeticao.getPaisA());
        Pais paisB  = paisService.retornarPais(formCompeticao.getPaisB());

        validacoesCompeticao(formCompeticao);

        Competicao competicao = formCompeticao.converter(local,etapa,modalidade,paisA,paisB);

        competicao.setLocal(local);
        competicao.setEtapa(etapa);
        competicao.setModalidade(modalidade);
        competicao.setPaisA(paisA);
        competicao.setPaisB(paisB);
        competicao.setDataInicio(formCompeticao.getDataInicio());
        competicao.setDataTermino(formCompeticao.getDataTermino());

        competicaoRepository.save(competicao);
        return new CompeticaoDto(competicao);
    }


    public Competicao atualizarCompeticao(Long id, CompeticaoForm formCompeticao){
        Local local = localService.retornarLocal(formCompeticao.getLocal());
        Etapa etapa = etapaService.retornarEtapa(formCompeticao.getEtapa());
        Modalidade modalidade = modalidadeService.retornarModalidade(formCompeticao.getModalidade());
        Pais paisA  = paisService.retornarPais(formCompeticao.getPaisA());
        Pais paisB  = paisService.retornarPais(formCompeticao.getPaisB());

        validacoesCompeticao(formCompeticao);
        
        buscarCompeticao(id);

        Competicao competicao = competicaoRepository.getOne(id);;
        competicao.setLocal(local);
        competicao.setEtapa(etapa);
        competicao.setModalidade(modalidade);
        competicao.setPaisA(paisA);
        competicao.setPaisB(paisB);
        competicao.setDataInicio(formCompeticao.getDataInicio());
        competicao.setDataTermino(formCompeticao.getDataTermino());

        return competicao;
    }

    public void excluirCompeticao(Long id){
        buscarCompeticao(id);
        competicaoRepository.deleteById(id);
    }

    private void validacoesCompeticao(CompeticaoForm formCompeticao){
        validaCamposData(formCompeticao.getDataInicio(),formCompeticao.getDataTermino());
        validaTempoCompeticao(formCompeticao.getDataInicio(),formCompeticao.getDataTermino());
        validaPeriodoLocalModalidade(formCompeticao.getDataInicio(),formCompeticao.getDataTermino(),formCompeticao.getLocal(),formCompeticao.getModalidade());
        validaMesmoPais(formCompeticao.getModalidade(),formCompeticao.getEtapa(),formCompeticao.getPaisA(),formCompeticao.getPaisB());
        validaCompeticaoDia(formCompeticao.getDataInicio(),formCompeticao.getLocal());
    }

    private Competicao buscarCompeticao(Long id){
        Optional<Competicao> competicao = competicaoRepository.findById(id);
        if (competicao.isPresent()){
            return competicao.get();
        }
        throw new ErroConteudoNaoEncontrado("id", "Competicao nao encontrada");
    }

    private void validaCamposData(LocalDateTime inicio, LocalDateTime termino){
        if(inicio.isAfter(termino)) {
            throw new ErroConteudoNaoEncontrado("dataInicio e dataTermino", "Data/Hora de termino deve ser maior que Data/Hora de inicio");
        }
    }

    private void validaTempoCompeticao(LocalDateTime inicio, LocalDateTime termino){
        Duration dur = Duration.between(inicio,termino);
        double minutes =  dur.getSeconds() / 60;
        Long min = (long) minutes;

        if (minutes < 30){
            throw new ErroConteudoNaoEncontrado("dataInicio e dataTermino", "Qualquer competicao deve durar pelo menos 30M, duracao: " + min + "M");
        }
    }

    private void validaPeriodoLocalModalidade(LocalDateTime inicio, LocalDateTime termino, Long local, Long modalidade){
        boolean competicao = competicaoRepository.findByPeriodoLocalModalidade(inicio,termino,local,modalidade);
        if (competicao){
            throw new ErroConteudoNaoEncontrado("dataInicio, dataTermino, local e modalidade ", "Duas competições não podem ocorrer " +
                    "no mesmo período, no mesmo local e para a mesma modalidade");
        }
    }

    private void validaMesmoPais(Long modalidade, Long etapa, Long paisA, Long paisB){
       String permitido = modalidadeService.retornarModalidade(modalidade).getPermitidoMesmoPais();
       if (paisA == paisB){
           if (permitido.contains("N") || etapa < 4){
               throw new ErroConteudoNaoEncontrado("paisA e paisB", "Para disputa do mesmo pais, a etapa deve ser Semifinal/Final" +
                       " e a modalidade deve possuir permissao!");
           }
       }
    }

    private void validaCompeticaoDia(LocalDateTime inicio, Long local){
        LocalDate data = inicio.toLocalDate();
        LocalDateTime dataHoraInicial = data.atStartOfDay();
        LocalDateTime dataHoraTermino = data.atTime(23,59,59);

        Long totalCompeticaoDia = competicaoRepository.countByTotalCompeticaoDia(dataHoraInicial,dataHoraTermino,local);

        if (totalCompeticaoDia >= 4){
            throw new ErroConteudoNaoEncontrado("dataInicio, dataTermino e local", "Limite de 4 competicoes por dia no local selecionado.");
        }
    }
}
