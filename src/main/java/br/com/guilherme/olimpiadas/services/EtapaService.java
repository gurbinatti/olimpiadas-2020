package br.com.guilherme.olimpiadas.services;

import br.com.guilherme.olimpiadas.config.validacao.ErroConteudoNaoEncontrado;
import br.com.guilherme.olimpiadas.controller.dto.EtapaDto;
import br.com.guilherme.olimpiadas.models.Etapa;
import br.com.guilherme.olimpiadas.repository.EtapaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EtapaService {
    @Autowired
    private EtapaRepository etapaRepository;

    public List<EtapaDto> buscarEtapas() {
        List<Etapa> etapas = etapaRepository.findAll();
        return EtapaDto.converter(etapas);
    }

    public Etapa retornarEtapa(Long id){
        return buscarEtapa(id);
    }

    private Etapa buscarEtapa(Long id) {
        Optional<Etapa> etapa = etapaRepository.findById(id);
        if (etapa.isPresent()) {
            return etapa.get();
        }
        throw new ErroConteudoNaoEncontrado("id", "Etapa nao encontrada");
    }
}

