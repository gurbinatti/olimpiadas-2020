package br.com.guilherme.olimpiadas.services;

import br.com.guilherme.olimpiadas.config.validacao.ErroConteudoNaoEncontrado;
import br.com.guilherme.olimpiadas.config.validacao.ErroDeFormularioException;
import br.com.guilherme.olimpiadas.controller.dto.ModalidadeDto;
import br.com.guilherme.olimpiadas.controller.form.ModalidadeForm;
import br.com.guilherme.olimpiadas.models.Modalidade;
import br.com.guilherme.olimpiadas.repository.ModalidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ModalidadeService {

    @Autowired
    private ModalidadeRepository modalidadeRepository;

    public Page<ModalidadeDto> buscarModalidades(String descricaoModalidade, Pageable paginacao) {
        if (descricaoModalidade == null) {
            return ModalidadeDto.converter(modalidadeRepository.findAll(paginacao));
        }
        return ModalidadeDto.converter(modalidadeRepository.findByDescricaoContains(descricaoModalidade, paginacao));
    }

    public ModalidadeDto inserirModalidade(ModalidadeForm formModalidade) {
        Long modalidadeId = 0L;
        validaModalidadeJaExiste(modalidadeId,formModalidade.getDescricao());
        validaPermitidoModalidade(formModalidade.getPermitidoMesmoPais());
        Modalidade modalidade = formModalidade.converter();
        modalidadeRepository.save(modalidade);
        return new ModalidadeDto(modalidade);
    }

    public void validaPermitidoModalidade(String permitido){
        permitido = permitido.toUpperCase().trim();

        if ((permitido.contains("S") || permitido.contains("N")) && permitido.length() == 1){
           return;
        }
        throw new ErroDeFormularioException("permitidoMesmoPais", "Deve ser S ou N");
    }

    public Modalidade retornarModalidade(Long id){
        return buscarModalidade(id);
    }

    public Modalidade atualizarModalidade(Long id, ModalidadeForm formModalidade){
        validaModalidadeJaExiste(id,formModalidade.getDescricao());
        validaPermitidoModalidade(formModalidade.getPermitidoMesmoPais());
        buscarModalidade(id);
        return formModalidade.atualizar(id,modalidadeRepository);
    }

    public void excluirModalidade(Long id){
        buscarModalidade(id);
        modalidadeRepository.deleteById(id);
    }

    private Modalidade buscarModalidade(Long id) {
        Optional<Modalidade> modalidade = modalidadeRepository.findById(id);
        if (modalidade.isPresent()) {
            return modalidade.get();
        }
        throw new ErroConteudoNaoEncontrado("id", "Modalidade nao encontrada");
    }

    public void validaModalidadeJaExiste(Long modalidadeId,String modalidadeDescricao) {
        Optional<Modalidade> modalidade = modalidadeRepository.findByDescricao(modalidadeDescricao);
        if (!modalidade.isEmpty()){
            if (modalidade.get().getId() != modalidadeId && modalidade.isPresent()){
                throw new ErroDeFormularioException("descricao", "Descricao ja existe");
            }
        }
    }

}
