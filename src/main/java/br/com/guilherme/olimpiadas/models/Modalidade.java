package br.com.guilherme.olimpiadas.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Data
public class Modalidade {
    @Id  @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(unique=true)
    private String descricao;

    private String permitidoMesmoPais;

    public Modalidade(){
    }

    public Modalidade(String descricao, String permitido){
        this.descricao = descricao;
        this.permitidoMesmoPais = permitido;
    }
}
