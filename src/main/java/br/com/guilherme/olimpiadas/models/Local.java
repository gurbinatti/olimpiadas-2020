package br.com.guilherme.olimpiadas.models;

import lombok.Data;
import lombok.Getter;
import org.springframework.stereotype.Service;

import javax.persistence.*;

@Entity
@Getter @Service
@Data
public class Local {
    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @Column(unique=true)
    private String descricao;

   public Local(){
    }

    public Local(String descricao){ this.descricao = descricao; }

}
