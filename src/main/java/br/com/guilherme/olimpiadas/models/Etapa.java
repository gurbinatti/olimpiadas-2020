package br.com.guilherme.olimpiadas.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Data
public class Etapa {
    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(unique=true)
    private String descricao;

    public  Etapa(){
    }

    public Etapa(String descricao){
        this.descricao = descricao;
    }
}