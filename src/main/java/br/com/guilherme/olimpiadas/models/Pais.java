package br.com.guilherme.olimpiadas.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Data
public class Pais {
    @Id  @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(unique=true)
    private String descricao;

    public Pais(){
    }

    public Pais(String descricao){
        this.descricao = descricao;
    }
}
