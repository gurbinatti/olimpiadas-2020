package br.com.guilherme.olimpiadas.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Data
public class Competicao {
    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime dataInicio;

    private LocalDateTime dataTermino;

    @ManyToOne
    private Etapa etapa;

    @ManyToOne
    private Modalidade modalidade;

    @ManyToOne
    private Local local;

    @ManyToOne
    private Pais paisA;

    @ManyToOne
    private Pais paisB;

    public Competicao(){
    }

    public Competicao(LocalDateTime dataInicio, LocalDateTime dataTermino, Local local , Etapa etapa, Modalidade modalidade, Pais paisA, Pais paisB) {
        this.dataInicio = dataInicio;
        this.dataTermino = dataTermino;
        this.local  = local;
        this.etapa = etapa;
        this.modalidade = modalidade;
        this.paisA = paisA;
        this.paisB = paisB;
    }
}
