package br.com.guilherme.olimpiadas.controller.form;

import br.com.guilherme.olimpiadas.models.Pais;
import br.com.guilherme.olimpiadas.repository.PaisRepository;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PaisForm {
    @NotNull @NotEmpty
    private String descricao;

    public Pais converter() { return new Pais(descricao); }

    public Pais atualizar(Long id, PaisRepository paisRepository) {
        Pais pais = paisRepository.getOne(id);
        pais.setDescricao(this.descricao);

        return pais;
    }
}
