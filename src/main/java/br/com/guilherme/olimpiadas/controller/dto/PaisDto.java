package br.com.guilherme.olimpiadas.controller.dto;

import br.com.guilherme.olimpiadas.models.Pais;
import lombok.Setter;
import org.springframework.data.domain.Page;
import lombok.Getter;

@Getter
@Setter
public class PaisDto {
    private Long id;

    private String descricao;

    public PaisDto(Pais pais)
    {
        setId(pais.getId());
        setDescricao(pais.getDescricao());
    }

    public static Page<PaisDto> converter(Page<Pais> paises){
        return  paises.map(PaisDto::new);
    }

    public static PaisDto converter(Pais pais){
        return new PaisDto(pais);
    }
}
