package br.com.guilherme.olimpiadas.controller.form;

import br.com.guilherme.olimpiadas.models.*;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class CompeticaoForm {
    @NotNull
    private LocalDateTime dataInicio;

    @NotNull
    private LocalDateTime dataTermino;

    @NotNull
    private Long local;

    @NotNull
    private Long etapa;

    @NotNull
    private Long Modalidade;

    @NotNull
    private Long paisA;

    @NotNull
    private  Long paisB;

    public Competicao converter(Local local, Etapa etapa, Modalidade modalidade, Pais pAA, Pais pBB)
    {
        return new Competicao(dataInicio,dataTermino,local,etapa,modalidade,pAA,pBB);
    }
}
