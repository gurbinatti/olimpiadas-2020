package br.com.guilherme.olimpiadas.controller;

import br.com.guilherme.olimpiadas.controller.dto.PaisDto;
import br.com.guilherme.olimpiadas.controller.form.PaisForm;
import br.com.guilherme.olimpiadas.repository.PaisRepository;
import br.com.guilherme.olimpiadas.services.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/olimpiadas/pais")
public class PaisController {
    @Autowired
    private PaisRepository paisRepository;

    @Autowired
    private PaisService paisService;

    @GetMapping
    public Page<PaisDto> lista(@RequestParam(required = false) String descricaoPais, Pageable paginacao) {
       return paisService.buscarPaises(descricaoPais, paginacao);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PaisDto> detalhar(@PathVariable Long id){
        return ResponseEntity.ok(new PaisDto(paisService.retornarPais(id)));
    }

    @PostMapping
    @Transactional
    public ResponseEntity<PaisDto> cadastrar(@RequestBody @Valid PaisForm formPais, UriComponentsBuilder uriBuilder){
        PaisDto paisDto = paisService.inserirPais(formPais);
        URI uri = uriBuilder.path("/olimpiadas/pais/{id}").buildAndExpand(paisDto.getId()).toUri();
        return ResponseEntity.created(uri).body(paisDto);
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<PaisDto> atualizar(@PathVariable Long id, @RequestBody @Valid PaisForm formPais)
    {
        return ResponseEntity.ok(new PaisDto(paisService.atualizarPais(id,formPais)));
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity remover(@PathVariable Long id){
        paisService.excluirPais(id);
        return ResponseEntity.ok().build();
    }
}