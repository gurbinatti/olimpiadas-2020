package br.com.guilherme.olimpiadas.controller.dto;


import br.com.guilherme.olimpiadas.models.Competicao;
import br.com.guilherme.olimpiadas.models.Etapa;
import br.com.guilherme.olimpiadas.models.Modalidade;
import br.com.guilherme.olimpiadas.models.Pais;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;

@Getter
@Setter
public class CompeticaoDto {
    private Long id;
    private LocalDateTime dataInicio;
    private LocalDateTime dataTermino;
    private Etapa etapa;
    private Modalidade modalidade;
    private Pais paisA;
    private Pais paisB;

    public CompeticaoDto(Competicao competicao){
        setId(competicao.getId());
        setDataInicio(competicao.getDataInicio());
        setDataTermino(competicao.getDataTermino());
        setEtapa(competicao.getEtapa());
        setModalidade(competicao.getModalidade());
        setPaisA(competicao.getPaisA());
        setPaisB(competicao.getPaisB());
    }
    public static Page<CompeticaoDto> converter(Page<Competicao> competicao){
        return  competicao.map(CompeticaoDto::new);
    }

    public static CompeticaoDto converter(Competicao competicao){
        return new CompeticaoDto(competicao);
    }
}
