package br.com.guilherme.olimpiadas.controller.dto;

import br.com.guilherme.olimpiadas.models.Local;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

@Getter
@Setter
public class LocalDto {
    private Long id;

    private String descricao;

    public LocalDto(Local local){
        setId(local.getId());
        setDescricao(local.getDescricao());
    }

    public static Page<LocalDto> converter(Page<Local> local){
        return  local.map(LocalDto::new);
    }

    public static LocalDto converter(Local local){
        return new LocalDto(local);
    }
}
