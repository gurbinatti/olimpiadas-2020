package br.com.guilherme.olimpiadas.controller;

import br.com.guilherme.olimpiadas.controller.dto.LocalDto;
import br.com.guilherme.olimpiadas.controller.form.LocalForm;
import br.com.guilherme.olimpiadas.repository.LocalRepository;
import br.com.guilherme.olimpiadas.services.LocalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/olimpiadas/local")
public class LocalController {
    @Autowired
    private LocalRepository localRepository;

    @Autowired
    private LocalService localService;

    @GetMapping
    public Page<LocalDto> lista(@RequestParam(required = false) String descricaoLocal, Pageable paginacao) {
        return localService.buscarLocais(descricaoLocal, paginacao);
    }

    @GetMapping("/{id}")
    public ResponseEntity<LocalDto> detalhar(@PathVariable Long id){
        return ResponseEntity.ok(new LocalDto(localService.retornarLocal(id)));
    }

    @PostMapping
    @Transactional
    public ResponseEntity<LocalDto> cadastrar(@RequestBody @Valid LocalForm formLocal, UriComponentsBuilder uriBuilder){
        LocalDto localDto = localService.inserirLocal(formLocal);
        URI uri = uriBuilder.path("/olimpiadas/local/{id}").buildAndExpand(localDto.getId()).toUri();
        return ResponseEntity.created(uri).body(localDto);
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<LocalDto> atualizar(@PathVariable Long id, @RequestBody @Valid LocalForm formLocal)
    {
        return ResponseEntity.ok(new LocalDto(localService.atualizarLocal(id,formLocal)));
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity remover(@PathVariable Long id){
        localService.excluirLocal(id);
        return ResponseEntity.ok().build();
    }
}