package br.com.guilherme.olimpiadas.controller;

import br.com.guilherme.olimpiadas.controller.dto.ModalidadeDto;
import br.com.guilherme.olimpiadas.controller.form.ModalidadeForm;
import br.com.guilherme.olimpiadas.repository.ModalidadeRepository;
import br.com.guilherme.olimpiadas.services.ModalidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/olimpiadas/modalidade")
public class ModalidadeController {
    @Autowired
    private ModalidadeRepository modalidadeRepository;

    @Autowired
    private ModalidadeService modalidadeService;

    @GetMapping
    public Page<ModalidadeDto> lista(@RequestParam(required = false) String descricaoModalidade, Pageable paginacao) {
        return modalidadeService.buscarModalidades(descricaoModalidade, paginacao);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ModalidadeDto> detalhar(@PathVariable Long id){
        return ResponseEntity.ok(new ModalidadeDto(modalidadeService.retornarModalidade(id)));
    }

    @PostMapping
    @Transactional
    public ResponseEntity<ModalidadeDto> cadastrar(@RequestBody @Valid ModalidadeForm formModalidade, UriComponentsBuilder uriBuilder){
        ModalidadeDto modalidadeDto = modalidadeService.inserirModalidade(formModalidade);
        URI uri = uriBuilder.path("/olimpiadas/modalidade/{id}").buildAndExpand(modalidadeDto.getId()).toUri();
        return ResponseEntity.created(uri).body(modalidadeDto);
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<ModalidadeDto> atualizar(@PathVariable Long id, @RequestBody @Valid ModalidadeForm formModalidade)
    {
        return ResponseEntity.ok(new ModalidadeDto(modalidadeService.atualizarModalidade(id,formModalidade)));
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity remover(@PathVariable Long id){
        modalidadeService.excluirModalidade(id);
        return ResponseEntity.ok().build();
    }
}