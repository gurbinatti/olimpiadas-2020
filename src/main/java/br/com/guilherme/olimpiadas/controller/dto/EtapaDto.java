package br.com.guilherme.olimpiadas.controller.dto;

import br.com.guilherme.olimpiadas.models.Etapa;
import lombok.Setter;
import org.springframework.data.domain.Page;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;


@Getter
@Setter
public class EtapaDto {
    private Long id;

    private String descricao;

    public EtapaDto(Etapa etapa)
    {
        setId(etapa.getId());
        setDescricao(etapa.getDescricao());
    }

    public static List<EtapaDto> converter(List<Etapa> etapas){
        return  etapas.stream().map(EtapaDto::new).collect(Collectors.toList());
    }
}
