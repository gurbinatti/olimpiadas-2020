package br.com.guilherme.olimpiadas.controller.form;

import br.com.guilherme.olimpiadas.models.Modalidade;
import br.com.guilherme.olimpiadas.repository.ModalidadeRepository;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ModalidadeForm {
    @NotNull @NotEmpty
    private String descricao;

    private String permitidoMesmoPais;

    public Modalidade converter() {
        return new Modalidade(descricao,permitidoMesmoPais);
    }

    public Modalidade atualizar(Long id, ModalidadeRepository modalidadeRepository) {
        Modalidade modalidade = modalidadeRepository.getOne(id);
        modalidade.setDescricao(this.descricao);
        modalidade.setPermitidoMesmoPais(this.permitidoMesmoPais);
        return modalidade;
    }
}
