package br.com.guilherme.olimpiadas.controller;

import br.com.guilherme.olimpiadas.controller.dto.EtapaDto;
import br.com.guilherme.olimpiadas.repository.EtapaRepository;

import br.com.guilherme.olimpiadas.services.EtapaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/olimpiadas/etapa")
public class EtapaController {
    @Autowired
    private EtapaRepository etapaRepository;

    @Autowired
    private EtapaService etapaService;

    @GetMapping
    public List<EtapaDto> lista() {
        return etapaService.buscarEtapas();
    }
}
