package br.com.guilherme.olimpiadas.controller.form;

import br.com.guilherme.olimpiadas.models.Local;
import br.com.guilherme.olimpiadas.repository.LocalRepository;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LocalForm {
    @NotNull @NotEmpty
    private String descricao;

    public Local converter() { return new Local(descricao); }

    public Local atualizar(Long id, LocalRepository localRepository) {
        Local local = localRepository.getOne(id);
        local.setDescricao(this.descricao);

        return local;
    }
}