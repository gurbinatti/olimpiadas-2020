package br.com.guilherme.olimpiadas.controller.dto;

import br.com.guilherme.olimpiadas.models.Modalidade;
import lombok.Setter;
import org.springframework.data.domain.Page;
import lombok.Getter;

@Getter
@Setter
public class ModalidadeDto {
    private Long id;

    private String descricao;

    private String permitidoMesmoPais;

    public ModalidadeDto(Modalidade modalidade)
    {
        setId(modalidade.getId());
        setDescricao(modalidade.getDescricao());
        setPermitidoMesmoPais(modalidade.getPermitidoMesmoPais());
    }

    public static Page<ModalidadeDto> converter(Page<Modalidade> modalidade){
        return  modalidade.map(ModalidadeDto::new);
    }

    public static ModalidadeDto converter(Modalidade modalidade){
        return new ModalidadeDto(modalidade);
    }
}
