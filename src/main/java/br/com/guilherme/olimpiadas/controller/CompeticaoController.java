package br.com.guilherme.olimpiadas.controller;

import br.com.guilherme.olimpiadas.controller.dto.CompeticaoDto;
import br.com.guilherme.olimpiadas.controller.form.CompeticaoForm;
import br.com.guilherme.olimpiadas.repository.CompeticaoRepository;
import br.com.guilherme.olimpiadas.services.CompeticaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;


@RestController
@RequestMapping("/olimpiadas/competicao")
public class CompeticaoController {
    @Autowired
    private CompeticaoRepository competicaoRepository;

    @Autowired
    private CompeticaoService competicaoService;

    @GetMapping
    @Cacheable(value="listaDeCompeticao")
    public Page<CompeticaoDto> lista(@RequestParam(required = false) Long modalidade, Pageable paginacao){
        return competicaoService.buscarCompeticoes(modalidade, paginacao);
   }

    @PostMapping
    @Transactional
    @CacheEvict(value="listaDeCompeticao", allEntries = true)
    public ResponseEntity<CompeticaoDto> cadastrar(@RequestBody @Valid CompeticaoForm formCompeticao, UriComponentsBuilder uriBuilder){
        CompeticaoDto competicaoDto = competicaoService.inserirCompeticao(formCompeticao);
        URI uri = uriBuilder.path("/olimpiadas/competicao/{id}").buildAndExpand(competicaoDto.getId()).toUri();
        return ResponseEntity.created(uri).body(competicaoDto);
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value="listaDeCompeticao", allEntries = true)
    public ResponseEntity<CompeticaoDto> atualizar(@PathVariable Long id, @RequestBody @Valid CompeticaoForm formCompeticao)
    {
        return ResponseEntity.ok(new CompeticaoDto(competicaoService.atualizarCompeticao(id,formCompeticao)));
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value="listaDeCompeticao", allEntries = true)
    public ResponseEntity remover(@PathVariable Long id){
        competicaoService.excluirCompeticao(id);
        return ResponseEntity.ok().build();
    }

}
