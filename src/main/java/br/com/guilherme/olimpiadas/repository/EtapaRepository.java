package br.com.guilherme.olimpiadas.repository;

import br.com.guilherme.olimpiadas.models.Etapa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtapaRepository extends JpaRepository<Etapa, Long> {

}
