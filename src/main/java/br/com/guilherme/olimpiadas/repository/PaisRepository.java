package br.com.guilherme.olimpiadas.repository;

import br.com.guilherme.olimpiadas.models.Pais;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Optional;

public interface PaisRepository extends JpaRepository<Pais, Long> {
    Page<Pais> findByDescricaoContains(String descricaoPais, Pageable paginacao);
    Optional<Pais> findByDescricao(String descricaoPais);
}
