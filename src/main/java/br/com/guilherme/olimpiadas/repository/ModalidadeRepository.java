package br.com.guilherme.olimpiadas.repository;

import br.com.guilherme.olimpiadas.models.Modalidade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ModalidadeRepository extends JpaRepository<Modalidade, Long> {
    Page<Modalidade> findByDescricaoContains(String descricaoModalidade, Pageable paginacao);
    Optional<Modalidade> findByDescricao(String descricaoModalidade);
}