package br.com.guilherme.olimpiadas.repository;

import br.com.guilherme.olimpiadas.models.Local;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Optional;

public interface LocalRepository extends JpaRepository<Local, Long> {
    Page<Local> findByDescricaoContains(String descricaoLocal, Pageable paginacao);
    Optional<Local> findByDescricao(String descricaoPais);
}
