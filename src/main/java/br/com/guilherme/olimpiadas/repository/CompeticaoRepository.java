package br.com.guilherme.olimpiadas.repository;

import br.com.guilherme.olimpiadas.models.Competicao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface CompeticaoRepository extends JpaRepository<Competicao, Long> {
     Page<Competicao> findByModalidade_Id(Long modalidade, Pageable paginacao);

     @Query(value="select case when count(*)> 0 then true else false end from Competicao where ((DATA_INICIO <= :dataInicio and DATA_TERMINO >= :dataInicio) OR (DATA_INICIO <= :dataTermino and DATA_TERMINO >= :dataTermino)) and LOCAL_ID = :local and MODALIDADE_ID = :modalidade",nativeQuery = true)
     boolean findByPeriodoLocalModalidade(@Param("dataInicio") LocalDateTime dataInicio, @Param("dataTermino") LocalDateTime dataTermino, @Param("local") Long local, @Param("modalidade") Long modalidade);

     @Query(value="select count(*) from Competicao where DATA_INICIO >= :dataInicio and DATA_TERMINO <= :dataTermino and LOCAL_ID = :local",nativeQuery = true)
     Long countByTotalCompeticaoDia(@Param("dataInicio") LocalDateTime dataInicio, @Param("dataTermino")LocalDateTime dataTermino, @Param("local") Long local);

}
