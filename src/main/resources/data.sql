insert into Etapa(descricao) values('Eliminatorias');
insert into Etapa(descricao) values('Oitavas de Final');
insert into Etapa(descricao) values('Quartas de Final');
insert into Etapa(descricao) values('Semifinal');
insert into Etapa(descricao) values('Final');

insert into Pais(descricao) values('Brasil');
insert into Pais(descricao) values('Argentina');
insert into Pais(descricao) values('Alemanha');
insert into Pais(descricao) values('Inglaterra');

insert into Modalidade(descricao,permitido_mesmo_pais) values ('Futebol','N');
insert into Modalidade(descricao,permitido_mesmo_pais) values ('Judo','S');

insert into LOCAL(descricao) values ('Novo Estadio Nacional de Toquio');
insert into LOCAL(descricao) values ('Nippon Budokan');
insert into LOCAL(descricao) values ('Centro Aquático de Tóquio');

insert into Competicao(data_inicio,data_termino,etapa_id,local_id,modalidade_id,paisa_id,paisb_id) values('2020-02-17 14:00:06.52','2020-02-17 14:59:59.52',1,1,1,1,2);
insert into Competicao(data_inicio,data_termino,etapa_id,local_id,modalidade_id,paisa_id,paisb_id) values('2020-02-17 15:00:06.52','2020-02-17 15:59:59.52',2,1,2,3,4);
insert into Competicao(data_inicio,data_termino,etapa_id,local_id,modalidade_id,paisa_id,paisb_id) values('2020-02-17 16:00:06.52','2020-02-17 16:59:59.52',4,1,2,4,4);
insert into Competicao(data_inicio,data_termino,etapa_id,local_id,modalidade_id,paisa_id,paisb_id) values('2020-02-17 17:00:06.52','2020-02-17 17:59:59.52',5,1,2,1,2);