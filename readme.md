# Commons API #
This project was developed to manage data from the Tokyo 2020 Olympic Games competitions.

### Requires ###
* Java 13
* Maven - https://maven.apache.org/download.cgi

### Start application server ###
```sh
mvn spring-boot:run
```

After the application started access the url:
```sh
http://localhost:8080/swagger-ui.html
```
You should see the swegger documentation

#### The postman collections test file its located under src/main/postman ####